package com.applaudo.challenge;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/app")
public class MainApplication extends Application {

}