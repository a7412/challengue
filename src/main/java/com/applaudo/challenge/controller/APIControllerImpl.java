package com.applaudo.challenge.controller;

import com.applaudo.challenge.ejb.remote.item.ItemEJB;
import com.applaudo.challenge.model.Item;
import com.applaudo.challenge.resource.RequestCreateItem;
import com.applaudo.challenge.resource.ResponseItem;
import com.applaudo.challenge.utils.Utils;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Path("/item")
public class APIControllerImpl {

    @EJB
    ItemEJB ejb;

    @Inject
    Utils utils;

    /**
     * Create an item record in database if that item does not exist and return 201 status code and inserted data
     * return 400 status code if the provided item status is invalid or the item record already exists in the database
     *
     * @param requestCreateItem {@link RequestCreateItem}
     * @return {@link Response}
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createItem(RequestCreateItem requestCreateItem ) {
        if (!utils.isValidItemStatus(requestCreateItem.getItemStatus())) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        Item searchedItem = ejb.findById(requestCreateItem.getItemId());

        if(Objects.isNull(searchedItem)) {
            Item created = ejb.create(utils.formatFieldsFromRequestCreateItem(requestCreateItem));
            return Response.status(Response.Status.CREATED).entity(utils.mapFromItemToResponseItem(created)).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    /**
     * Get an item record from database by its id
     * If the item records exists return data and 200 status code
     * else return 404 status code
     *
     * @param itemId {@link Integer}
     * @return {@link Response}
     */
    @GET
    @Path("{itemId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findByItemId(@PathParam("itemId") int itemId) {
        Item searchedItem = ejb.findById(itemId);

        if(Objects.isNull(searchedItem)) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.status(Response.Status.OK).entity(utils.mapFromItemToResponseItem(searchedItem)).build();
        }
    }

    /**
     * Get a list of item records from database by an item status and an item name
     * These two query params are mandatory so if at least one of them is null it will return 400 status code
     * If the list is empty returns 404 status code and empty json array else return 200 status code and data
     *
     * @param itemStatus {@link String}
     * @param itemName {@link String}
     * @return {@link Response}
     */
    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findByItemStatusAndItemName(@QueryParam("itemStatus") String itemStatus,
                      @QueryParam("itemName") String itemName) {

        if(!utils.isValidQueryParamsForStatusAndName(itemStatus, itemName)) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        List<ResponseItem> data = ejb.findAllByStatusAndItemName(itemStatus, itemName)
                                            .stream().map(i -> utils.mapFromItemToResponseItem(i)).collect(Collectors.toList());

        Response.Status status = data.isEmpty() ? Response.Status.NOT_FOUND : Response.Status.OK;
        return Response.status(status).entity(data).build();
    }
}