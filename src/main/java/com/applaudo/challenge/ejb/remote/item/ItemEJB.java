package com.applaudo.challenge.ejb.remote.item;

import com.applaudo.challenge.model.Item;
import com.applaudo.challenge.resource.RequestCreateItem;
import javax.ejb.Remote;
import java.util.List;

@Remote
public interface ItemEJB {

    List<Item> findAllByStatusAndItemName(String status, String itemName);
    Item findById(int id);
    Item create(RequestCreateItem incoming);
}
