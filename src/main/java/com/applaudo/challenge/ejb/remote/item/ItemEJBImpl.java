package com.applaudo.challenge.ejb.remote.item;

import com.applaudo.challenge.PersistenceManager;
import com.applaudo.challenge.model.Item;
import com.applaudo.challenge.resource.RequestCreateItem;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;
import java.util.Locale;

@Stateless
@Remote
public class ItemEJBImpl implements ItemEJB {

    @Override
    public List<Item> findAllByStatusAndItemName(String status, String itemName) {
        EntityManager em = PersistenceManager.getInstance().createEntityManager();

        //lower case to match the case insensitive requirement
        Query query = em.createQuery("SELECT t FROM Item t where lower(t.itemStatus) = ?1 and lower(t.itemName) = ?2", Item.class);
        return query.setParameter(1, status.toLowerCase()).setParameter(2, itemName.toLowerCase()).getResultList();
    }

    @Override
    public Item findById(int id) {
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
        Query query = em.createQuery("SELECT t FROM Item t where t.itemId = ?1", Item.class);

        List<Item> data = query.setParameter(1, id).getResultList();
        return data.isEmpty() ? null : data.get(0);
    }

    @Override
    public Item create(RequestCreateItem incoming) {

        Item created = new Item(incoming.getItemId(),
                incoming.getItemName(),
                incoming.getItemEnteredByUser(),
                incoming.getItemEnteredDate(),
                incoming.getItemBuyingPrice(),
                incoming.getItemSellingPrice(),
                incoming.getItemLastModifiedDate(),
                incoming.getItemLastModifiedByUser(),
                incoming.getItemStatus());
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
        em.getTransaction().begin();
        em.persist(created);
        em.getTransaction().commit();

        Query query = em.createQuery("SELECT t FROM Item t where t.itemId = ?1", Item.class);
        return (Item) query.setParameter(1, incoming.getItemId()).getSingleResult();
    }
}