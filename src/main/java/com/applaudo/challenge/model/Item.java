package com.applaudo.challenge.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "Item")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Item implements Serializable {

    /**
     * Item Identifier
     */
    @Id
    @Column(name = "itemId", nullable = false, unique = true, updatable = false)
    private Integer itemId;

    /**
     * Item Name
     */
    @Column(name = "itemName")
    private String itemName;

    /**
     * Item Entered By User
     */
    @Column(name = "itemEnteredByUser")
    private String itemEnteredByUser;

    /**
     * Item Entered Date
     */
    @Column(name = "itemEnteredDate")
    private Date itemEnteredDate;

    /**
     * Item Buying Price
     */
    @Column(name = "itemBuyingPrice")
    private Double itemBuyingPrice;

    /**
     * Item Selling Price
     */
    @Column(name = "itemSellingPrice")
    private Double itemSellingPrice;

    /**
     * Item Last Modified Date
     */
    @Column(name = "itemLastModifiedDate")
    private Date itemLastModifiedDate;

    /**
     * Item Last Modified By User
     */
    @Column(name = "itemLastModifiedByUser")
    private String itemLastModifiedByUser;

    /**
     * Item Status
     */
    @Column(name = "itemStatus")
    private String itemStatus;
}