package com.applaudo.challenge.resource;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RequestCreateItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @JsonProperty("itemId")
    private Integer itemId;

    @NotNull
    @JsonProperty("itemName")
    private String itemName;

    @NotNull
    @JsonProperty("itemEnteredByUser")
    private String itemEnteredByUser;

    @NotNull
    @JsonProperty("itemEnteredDate")
    private Date itemEnteredDate;

    @NotNull
    @JsonProperty("itemBuyingPrice")
    private Double itemBuyingPrice;

    @NotNull
    @JsonProperty("itemSellingPrice")
    private Double itemSellingPrice;

    @NotNull
    @JsonProperty("itemLastModifiedDate")
    private Date itemLastModifiedDate;

    @NotNull
    @JsonProperty("itemLastModifiedByUser")
    private String itemLastModifiedByUser;

    @NotNull
    @JsonProperty("itemStatus")
    private String itemStatus;
}
