package com.applaudo.challenge.utils;

public class Constants {
    public final static String ITEM_STATUS_AVAILABLE = "AVAILABLE";
    public final static String ITEM_STATUS_SOLD = "SOLD";
}
