package com.applaudo.challenge.utils;

import com.applaudo.challenge.model.Item;
import com.applaudo.challenge.resource.RequestCreateItem;
import com.applaudo.challenge.resource.ResponseItem;

import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Objects;

public class Utils {

    public boolean isValidItemStatus(String itemStatus) {
        //only constraint is for the itemStatus field
        return itemStatus.equalsIgnoreCase(Constants.ITEM_STATUS_AVAILABLE) ||
                itemStatus.equalsIgnoreCase(Constants.ITEM_STATUS_SOLD);
    }

    public RequestCreateItem formatFieldsFromRequestCreateItem(RequestCreateItem requestCreateItem) {
        //format double values up to one decimal place
        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.CEILING);

        double formatItemBuyingPrice = Double.parseDouble(df.format(requestCreateItem.getItemBuyingPrice()));
        double formatItemSellingPrice = Double.parseDouble(df.format(requestCreateItem.getItemSellingPrice()));

        requestCreateItem.setItemBuyingPrice(formatItemBuyingPrice);
        requestCreateItem.setItemSellingPrice(formatItemSellingPrice);
        return requestCreateItem;
    }

    public ResponseItem mapFromItemToResponseItem(Item item) {

        Timestamp timestEnterDate = new Timestamp(item.getItemEnteredDate().getTime());
        Timestamp timestLastModifDate = new Timestamp(item.getItemLastModifiedDate().getTime());

        return new ResponseItem(
                item.getItemId(),
                item.getItemName(),
                item.getItemEnteredByUser(),
                timestEnterDate.toString(),
                item.getItemBuyingPrice(),
                item.getItemSellingPrice(),
                timestLastModifDate.toString(),
                item.getItemLastModifiedByUser(),
                item.getItemStatus()
        );
    }

    public boolean isValidQueryParamsForStatusAndName(String itemStatus, String itemName) {
        return !Objects.isNull(itemStatus) && !Objects.isNull(itemName) && isValidItemStatus(itemStatus);
    }
}
