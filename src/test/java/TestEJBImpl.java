import com.applaudo.challenge.model.Item;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.text.ParseException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
public class TestEJBImpl {

    private static final ObjectMapper om = new ObjectMapper();

    private Map<String, Item> getTestCreateData() throws ParseException {
        Map<String, Item> data = new LinkedHashMap<>();

        Item chicago = new Item( 1,
                "item_x",
                "user_x",
                new Date("2020-05-10T13:00:41.499"),
                850.0,
                355.0,
                new Date("2020-05-10T13:00:41.498"),
                "user_y",
                "AVAILABLE"
                );
        data.put("itemId1", chicago);
        return data;
    }


}
